// import logo from './logo.svg';
import './App.css';
import React from 'react'

function App() {
  const [state, setState] = React.useState(navigator.userAgent)
  return (
    <div className="App">
      <header className="App-header">

        <h1>
          Welcome to 2024, This is a React App for VOLVO Platform Engineer.
        </h1>
        <p>
          {state}
        </p>
      </header>
    </div>
  );
}

export default App;
