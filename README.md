# To Deploy a React APP to print user agent info and some text

## Steps to create React APP and print info

```bash
npx create react-app <name-of-app>

npm install

npm run start   # THis will start the APP

```

## To create VPC and Elastic Kubernetes Service we take help from Terraform

**We use modules from the github public repo which are part of my repo**

https://github.com/sidhusaiprasanth/vpc

https://github.com/sidhusaiprasanth/eks-cluster-2024

**To create this VPC and cluster we have actual settings and project in my gitlab repo mentioned below**

https://gitlab.com/sidhusaiprasanth/create-eks-cluster-terraform

```bash
#We are importing two modules https://github.com/sidhusaiprasanth/vpc, https://github.com/sidhusaiprasanth/eks-cluster-2024 

#in main.tf

module "vpc" {
  source       = "git::https://github.com/sidhusaiprasanth/vpc"
  cidr_block   = var.cidr_block
  env          = var.env
  cluster_name = var.cluster_name
  default_tags = var.default_tags
  pub_subnets  = var.pub_subnets
  pri_subnets  = var.pri_subnets
}


module "eks-cluster" {
  depends_on      = [module.vpc]
  source          = "git::https://github.com/sidhusaiprasanth/eks-cluster-2024"
  env             = var.env
  cluster_name    = var.cluster_name
  cluster_version = var.cluster_version
  pub_subnet_ids  = module.vpc.pub_subnets_ids_list
  pri_subnet_ids  = module.vpc.pri_subnets_ids_list
  default_tags    = var.default_tags

}


```

**Once VPC and EKS cluster are created, we are using the react code in this repo**

**We have a different repo where we maintain kubernetes manifests to follow the GitOps Approach, Repo where we maintain manifests is given below**

https://gitlab.com/sidhusaiprasanth/volvo-app-platform-engineer-k8s-manifests

**We will install ARGOCD in our EKS cluster to automate CD process**

![Portforwardingvolvoappservice](VOLVOAPPscreenshots/Screenshot%202024-03-06%20at%209.46.02 AM.png)

![PortforwardingArgoCDservice](VOLVOAPPscreenshots/Screenshot%202024-03-06%20at%209.45.26 AM.png)

![ArgoCDdeployingourapp](VOLVOAPPscreenshots/Screenshot%202024-03-06%20at%209.43.14 AM.png)

![ArgoCDdeployingourapp](VOLVOAPPscreenshots/Screenshot%202024-03-06%20at%209.43.50 AM.png)

![ArgoCDdeployingourapp](VOLVOAPPscreenshots/Screenshot%202024-03-06%20at%209.44.30 AM.png)

![Wecanviewourapp](VOLVOAPPscreenshots/Screenshot%202024-03-06%20at%209.45.01 AM.png)







